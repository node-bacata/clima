require('dotenv').config();

var colors = require('colors');
const Busquedas = require("./models/Busquedas")


const main = async () =>{

    const busquedas = new Busquedas();
    const ciudadResponse = await busquedas.buscarCiudad('Fusagasuga');
    imprimirInformacionCiudad(ciudadResponse); 
}



const imprimirInformacionCiudad = (ciudad) => {
    console.log('informacion de la ciudad buscada:'.red);
    console.log('Ciudad:', ciudad.nombre);
    console.log('Longitud:', ciudad.longitud);
    console.log('Latitud', ciudad.latitud);
    console.log('Temperatura', ciudad.temperaturaActual);
    console.log('Temperatura max', ciudad.temperaturaMax);
    console.log('Temperatura min', ciudad.temperaturaMin);
    console.log('Descripcion', ciudad.descripcion);
}

main();