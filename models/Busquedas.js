const axios = require("axios");

class Busquedas {


  constructor() {}

  get paramsMapBox() {
    return {
      access_token: process.env.MAPBOX_KEY,
      limit: 1,
      language: "es",
    };
  }
  get paramsWeather() {
    return {
        appid: process.env.OPENWEATHER_KEY,
        units: 'metric',
        lang: 'es'
    }
}

  async buscarCiudad(ciudadNombre) {
    try {
      const instanceMapBox = this.llamarMapBox(ciudadNombre);
      const responseCiudad = await instanceMapBox.get();

      const instanceWeather = this.llamarOpenWeather(
        responseCiudad.data.features[0].center[1],
        responseCiudad.data.features[0].center[0]
      );
      const responseWeather = await instanceWeather.get();
        console.log(responseWeather)
      return this.crearCiudad(responseCiudad.data.features[0], responseWeather.data.main, responseWeather.data.weather[0].description);
    } catch (error) {
      console.log(error);
    }
  }

  llamarMapBox(ciudadNombre) {
    return axios.create({
      baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${ciudadNombre}.json`,
      params: this.paramsMapBox,
    });
  }


  llamarOpenWeather(lat, lon) {
    return axios.create({
      baseURL: `https://api.openweathermap.org/data/2.5/weather`,
      params: { ...this.paramsWeather, lat, lon },
    });
  }

  crearCiudad({ id, place_name, center }, {temp, temp_max, temp_min }, descripcion ) {
    return {
      id: id,
      nombre: place_name,
      longitud: center[0],
      latitud: center[1],
      temperaturaMin: temp_min,
      temperaturaMax: temp_max,
      temperaturaActual: temp,
      descripcion: descripcion
    };
  }
  
}

module.exports = Busquedas;
