# App Clima - Bacata Dev

This is an application to check the weather of any city with its name.
## Used tools

 - [Axios](https://www.npmjs.com/package/axios): For API consume 
 - [Reqres](https://reqres.in/): For test API consume
 - [mapbox](https://www.mapbox.com/): For get city information
 - [Geocoding](https://docs.mapbox.com/api/search/geocoding/): For get doc of mapbox
 - [dotenv](https://www.npmjs.com/package/dotenv): For manage environment var
 - [openweathermap](https://home.openweathermap.org/): For get weather

> Pleace ejecute `npm install`


